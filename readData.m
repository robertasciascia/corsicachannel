% script to load data from the Corsica Channel (from current meter) and create a mat file 
% RS


% The file from the current meter has a structure 
% AEA128.I03  where AEA (Andreera), 128(instrument code) . I (month), 03 (year)
% To cove the year 2004 we nned 3 groups of records each group has 4 depth (NB one deppth is missing in one group)
% 
% 01/09/2003 - 09/05/2004 (complete record)
% 09/05/2004 - 25/09/2004 (one depth is missing)
% 25/09/2004 - 14/04/2005 (complete record)

% each record has an header with some info like (lat,lon, date of first record,last record, depth of the instrument, sea level depth, saveing interval)


clear all; close all;

addpath(genpath('/mnt/data2/sciascia/utilities/')) 

indir = ['/mnt/data2/ROMS/CorsChannelData/'];


% Read first file (depth 52m AEA128.I03)

inname=['AEA128.I03'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
data(:,:,1) = tmp(:,1:4);

clear fid d tmp 

% Read first file (depth 108m AEA127.I03)

inname=['AEA127.I03'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
data(:,:,2) = tmp(:,1:4);

clear fid d tmp 

% Read first file (depth 310m AEA11236.I03)

inname=['AEA11236.I03'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);

tmp=cell2mat(d);
data(:,:,3) = tmp(:,1:4);

clear fid d tmp 
 
% Read first file (depth 383m AEA11237.I03)

inname=['AEA11237.I03'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);

tmp=cell2mat(d);
data(:,:,4) = tmp(:,1:4);

clear fid d tmp 

% Read second file (depth 49m AEA128.E04)

inname=['AEA130.E04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

cc_data(:,:,1)=[squeeze(data(:,:,1));tmp];
cc_data(end+1,:,1)=NaN;

clear fid d tmp 


% Read second file (depth 100m is missing)

tmp=NaN(3330,4);

cc_data(:,:,2)=[squeeze(data(:,:,2));tmp];

clear tmp

% Read second file (depth 302m AEA9147.E04)

inname=['AEA9147.E04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp1=cell2mat(d);
tmp1= tmp1(:,1:4);
a=zeros(1178,4);
tmp=[tmp1;a];;
cc_data(:,:,3)=[squeeze(data(:,:,3));tmp];

clear fid d tmp tmp1 a

% Read second file (depth  401m AEA10012.E04)

inname=['AEA10012.E04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

cc_data(:,:,4)=[squeeze(data(:,:,4));tmp];

clear fid d tmp data 


% Read third  file (depth  51m AEA128.I04)

inname=['AEA128.I04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

CorsChan_data(:,:,1)=[squeeze(cc_data(:,:,1));tmp];

clear fid d tmp 

% Read third  file (depth  105m AEA127.I04)

inname=['AEA127.I04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

CorsChan_data(:,:,2)=[squeeze(cc_data(:,:,2));tmp];

clear fid d tmp 


% Read third  file (depth  307m AEA11236.I04)

inname=['AEA11236.I04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

CorsChan_data(:,:,3)=[squeeze(cc_data(:,:,3));tmp];

clear fid d tmp 

% Read third  file (depth  370m AEA11237.I04)

inname=['AEA11237.I04'];
infile=[indir,inname];
fid=fopen(infile);
d = textscan(fid, '%f%f%f%f%f', 'HeaderLines',5, 'Delimiter','\n', 'CollectOutput',1);
fclose(fid);
tmp=cell2mat(d);
tmp= tmp(:,1:4);

CorsChan_data(:,:,4)=[squeeze(cc_data(:,:,4));tmp];

clear fid d tmp cc_data

CorsChan_2004=NaN(8784,4,4);
CorsChan_2004(1:3104,:,:)=CorsChan_data(2911:6014,:,:);
CorsChan_2004(3111:(3111+3330),:,:)=CorsChan_data(6015:(6015+3330),:,:);
CorsChan_2004(6446:(6446+2338),:,:)=CorsChan_data(9345:(9345+2338),:,:);


tmp=reshape(CorsChan_2004,[24,366,4,4]);
CorsChan_daily_2004=squeeze(nanmean(tmp,1));

savename= ['CorsChann_2004.mat'];
savefile= [indir,savename];
save(savefile,'CorsChan_2004','CorsChan_daily_2004');

fg1=figure(1);

set(fg1,'Position', [102 643 1672 478],'PaperPositionMode','auto') 
plot(CorsChan_2004(:,1,1),'-k','LineWidth',1.5);hold on; 
plot(CorsChan_2004(:,2,1),'-r','LineWidth',1.5); hold off 
set(gca,'FontSize',12,'Fontname','Helvetica');
xlabel('2004 [hr]','FontSize',14);
ylabel('Velocity [cm/s]','FontSize',14);
legend('EW velocity','NS velocity')
title('Corsica Channel - Current Velocity at 50m - 2004','FontSize',14)

printname = ['Vel2004_OBS_hourly.png'];
printfile= [indir,printname];
print('-dpng','-r300',printfile);


fg2=figure(2);

set(fg2,'Position', [102 643 1672 478],'PaperPositionMode','auto') 
plot(CorsChan_daily_2004(:,1,1),'-k','LineWidth',1.5);hold on; 
plot(CorsChan_daily_2004(:,2,1),'-r','LineWidth',1.5); hold off 
set(gca,'FontSize',12,'Fontname','Helvetica');
xlabel('2004 [d]','FontSize',14);
ylabel('Velocity [cm/s]','FontSize',14);
legend('EW velocity','NS velocity') 
title('Corsica Channel - Current Velocity at 50m - 2004','FontSize',14)

printname = ['Vel2004_OBS_daily.png'];
printfile= [indir,printname];
print('-dpng','-r300',printfile);


u=squeeze(CorsChan_2004(:,1,1));
v=squeeze(CorsChan_2004(:,2,1));
tmax=366*24;
time=[1:1:tmax]';


u_daily=squeeze(CorsChan_daily_2004(:,1,2));
v_daily=squeeze(CorsChan_daily_2004(:,2,2));
dmax=366;
dtime=[1:1:dmax]';

figure(4)
set(4,'Position', [5 57 1014 632],'PaperPositionMode','auto');
set(gca,'Fontname','Helvetica','FontSize',12);
scale=0;
for ii=1:1:dmax
 hold on
 plot(dtime(ii),0,'k-')
 quiver(dtime(ii),0,u_daily(ii),v_daily(ii),scale,'b.')
end
quiver(310,100,20,0,0,'.b')
text(310,110,'20 cm/s')
hold off;
axis equal;
set(gca,'yticklabel','');
set(gca,'Xgrid','on')
xlabel('2004 [d]','FontSize',14);
ylabel('Current Velocity ','FontSize',14);
box on;
printname = ['StickPlot2004_100m_daily.png'];
printfile= [indir,printname];
print('-dpng','-r300',printfile);


